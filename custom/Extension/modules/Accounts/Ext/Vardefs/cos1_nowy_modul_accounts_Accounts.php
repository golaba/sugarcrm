<?php
// created: 2015-06-25 09:17:52
$dictionary["Account"]["fields"]["cos1_nowy_modul_accounts"] = array (
  'name' => 'cos1_nowy_modul_accounts',
  'type' => 'link',
  'relationship' => 'cos1_nowy_modul_accounts',
  'source' => 'non-db',
  'module' => 'cos1_nowy_modul',
  'bean_name' => 'cos1_nowy_modul',
  'vname' => 'LBL_COS1_NOWY_MODUL_ACCOUNTS_FROM_COS1_NOWY_MODUL_TITLE',
  'id_name' => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
);
$dictionary["Account"]["fields"]["cos1_nowy_modul_accounts_name"] = array (
  'name' => 'cos1_nowy_modul_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_COS1_NOWY_MODUL_ACCOUNTS_FROM_COS1_NOWY_MODUL_TITLE',
  'save' => true,
  'id_name' => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
  'link' => 'cos1_nowy_modul_accounts',
  'table' => 'cos1_nowy_modul',
  'module' => 'cos1_nowy_modul',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["cos1_nowy_modul_accountscos1_nowy_modul_ida"] = array (
  'name' => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
  'type' => 'link',
  'relationship' => 'cos1_nowy_modul_accounts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_COS1_NOWY_MODUL_ACCOUNTS_FROM_ACCOUNTS_TITLE',
);
