<?php
// created: 2015-06-25 09:17:52
$dictionary["cos1_nowy_modul"]["fields"]["cos1_nowy_modul_accounts"] = array (
  'name' => 'cos1_nowy_modul_accounts',
  'type' => 'link',
  'relationship' => 'cos1_nowy_modul_accounts',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'side' => 'right',
  'vname' => 'LBL_COS1_NOWY_MODUL_ACCOUNTS_FROM_ACCOUNTS_TITLE',
);
