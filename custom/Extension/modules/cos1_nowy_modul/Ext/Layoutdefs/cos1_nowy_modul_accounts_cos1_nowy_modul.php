<?php
 // created: 2015-06-25 09:17:52
$layout_defs["cos1_nowy_modul"]["subpanel_setup"]['cos1_nowy_modul_accounts'] = array (
  'order' => 100,
  'module' => 'Accounts',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_COS1_NOWY_MODUL_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'get_subpanel_data' => 'cos1_nowy_modul_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
