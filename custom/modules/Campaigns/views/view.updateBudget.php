<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

require_once('include/json_config.php');
require_once('include/MVC/View/views/view.detail.php');

class CustomCampaignsViewupdateBudget extends SugarView {

 	function CampaignsViewupdateBudget(){
        parent::SugarView();
 	}
 	

    function preDisplay(){
        $this->bean->tpl = 'custom/modules/Campaigns/tpl/updateBudget.tpl'; 
        $this->beanList = $this->bean->get_full_list();
        parent::preDisplay();
    }        

 	function display() {
        $smarty = new Sugar_Smarty();
        $camps = array();
        foreach($this->beanList as $camp){
            $camps[] = array('name' => $camp->name, 'id' => $camp->id);
        }
        $smarty->assign("camps", $camps);
        $smarty->display($this->bean->tpl);
        parent::display();  
    }
}
?>