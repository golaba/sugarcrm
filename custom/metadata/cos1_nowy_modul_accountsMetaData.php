<?php
// created: 2015-06-25 09:17:52
$dictionary["cos1_nowy_modul_accounts"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'cos1_nowy_modul_accounts' => 
    array (
      'lhs_module' => 'cos1_nowy_modul',
      'lhs_table' => 'cos1_nowy_modul',
      'lhs_key' => 'id',
      'rhs_module' => 'Accounts',
      'rhs_table' => 'accounts',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'cos1_nowy_modul_accounts_c',
      'join_key_lhs' => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
      'join_key_rhs' => 'cos1_nowy_modul_accountsaccounts_idb',
    ),
  ),
  'table' => 'cos1_nowy_modul_accounts_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'cos1_nowy_modul_accountsaccounts_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'cos1_nowy_modul_accountsspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'cos1_nowy_modul_accounts_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'cos1_nowy_modul_accountscos1_nowy_modul_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'cos1_nowy_modul_accounts_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'cos1_nowy_modul_accountsaccounts_idb',
      ),
    ),
  ),
);