<?php
$module_name = 'ebi_Ankieta';
$listViewDefs [$module_name] = 
array (
  'WYKORZ_ZASOW' => 
  array (
    'type' => 'enum',
    'studio' => 'visible',
    'label' => 'LBL_WYKORZ_ZASOW',
    'width' => '10%',
    'default' => true,
  ),
  'UZASADNIENIE' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_UZASADNIENIE',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'DOSTEP' => 
  array (
    'type' => 'text',
    'default' => true,
    'studio' => 'visible',
    'label' => 'LBL_DOSTEP',
    'sortable' => false,
    'width' => '10%',
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => true,
  ),
);
?>
