<?php
$module_name = 'ebi_Ankieta';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'wykorz_zasow' => 
      array (
        'type' => 'enum',
        'studio' => 'visible',
        'label' => 'LBL_WYKORZ_ZASOW',
        'width' => '10%',
        'default' => true,
        'name' => 'wykorz_zasow',
      ),
      'dostep' => 
      array (
        'type' => 'text',
        'default' => true,
        'studio' => 'visible',
        'label' => 'LBL_DOSTEP',
        'sortable' => false,
        'width' => '10%',
        'name' => 'dostep',
      ),
      'uzasadnienie' => 
      array (
        'type' => 'text',
        'studio' => 'visible',
        'label' => 'LBL_UZASADNIENIE',
        'sortable' => false,
        'width' => '10%',
        'default' => true,
        'name' => 'uzasadnienie',
      ),
      'current_user_only' => 
      array (
        'name' => 'current_user_only',
        'label' => 'LBL_CURRENT_USER_FILTER',
        'type' => 'bool',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      0 => 'name',
      1 => 
      array (
        'name' => 'assigned_user_id',
        'label' => 'LBL_ASSIGNED_TO',
        'type' => 'enum',
        'function' => 
        array (
          'name' => 'get_user_array',
          'params' => 
          array (
            0 => false,
          ),
        ),
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
