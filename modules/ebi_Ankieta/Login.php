<?php
    $camp = new Campaign();
    $camp->retrieve($_REQUEST['record']);
    if ($camp->status === 'Inactive') {
    	$camp->status = 'In Queue';
    } else
    $camp->status = 'Complete';
    $camp->save();

    header("Location: index.php?module=Campaigns&action=DetailView&record=" . $_REQUEST['record']);
    die();
?>