<?php
 class initial_call_class {
 	function initial_call_method($bean, $event, $arguments) {
 		if ($bean->fetched_row['id'] != $bean->id && $bean->do_not_call == false) {
			 $call = new Call();
			 $call->assigned_user_id = $bean->assigned_user_id;
			 $call->assigned_user_name = $bean->assigned_user_name;
			 $call->name = "Please call our new Lead, ".$bean->first_name." ".$bean->last_name .", at ".$bean->phone_work;
			 $call->direction = "Outbound";
			 $call->status = "Planned";
			 $call->date_start = gmdate("Y-m-d H:i:s", strtotime("+15 minutes"));
			 $call->duration_minutes = "15";
			 $call->parent_type = "Leads";
			 $call->parent_id = $bean->id;
			 $call->save();
		 }
 	}
 }
 ?>